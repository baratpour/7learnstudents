package com.sevenlearn.a7learnstudents;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    private MutableLiveData<List<Student>> students = new MutableLiveData<>();
    private MutableLiveData<String> error = new MutableLiveData<>();

    public MainViewModel(ApiService apiService) {
        apiService.getStudents().enqueue(new Callback<List<Student>>() {
            @Override
            public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {
                students.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Student>> call, Throwable t) {
                error.setValue("خطای نامشخص");
            }
        });
    }

    public LiveData<List<Student>> getStudents() {
        return students;
    }

    public LiveData<String> getError() {
        return error;
    }
}
